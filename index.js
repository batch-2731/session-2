//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

// //create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

/*
    QUIZ

    1. What is the term given to unorganized code that's very hard to work with?
		-Spaghetti Code

	2. How are object literals written in JS?
		-using curly braces or Object Literals '{}'

	3. What do you call the concept of organizing information and functionality to belong to an object?
		-Encapsulation

	4. If student1 has a method named enroll(), how would you invoke it?
		-student1.enroll()

	5. True or False: Objects can have objects as properties.
		-True

    6. What is the syntax in creating key-value pairs?
        - let myObject = { key1: value1, key2: value2, key3: value3 };
    
    7. True or False: A method can have no parameters and still work.
		-True

	8. True or False: Arrays can have objects as elements.
		-True

	9. True or False: Arrays are objects.
		-True. Arrays are special forms of objects.

	10. True or False: Objects can have arrays as properties.
		-True

*/

// Part 2 Function coding
// 1 to 4

let student1 = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],
  login: function () {
    console.log(`${this.email} has logged in`);
  },
  logout: function () {
    console.log(`${this.email} has logged out`);
  },
  listGrades: function () {
    this.grades.forEach((grade) => {
      console.log(grade);
    });
  },
  computeAve: function () {
    const ave = this.grades.reduce((a, b) => a + b) / this.grades.length;
    return ave;
  },
  willPass: function () {
    const ave = Math.round(this.computeAve());

    if (ave >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors: function () {
    const ave = Math.round(this.computeAve());

    if (ave >= 90) {
      return true;
    } else if (ave >= 85 && ave < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let student2 = {
  name: "Joe",
  email: "joe@mail.com",
  grades: [78, 82, 79, 85],
  login: function () {
    console.log(`${this.email} has logged in`);
  },
  logout: function () {
    console.log(`${this.email} has logged out`);
  },
  listGrades: function () {
    this.grades.forEach((grade) => {
      console.log(grade);
    });
  },
  computeAve: function () {
    const ave = this.grades.reduce((a, b) => a + b) / this.grades.length;
    return ave;
  },
  willPass: function () {
    const ave = Math.round(this.computeAve());

    if (ave >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors: function () {
    const ave = Math.round(this.computeAve());

    if (ave >= 90) {
      return true;
    } else if (ave >= 85 && ave < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let student3 = {
  name: "Jane",
  email: "jane@mail.com",
  grades: [87, 89, 91, 93],
  login: function () {
    console.log(`${this.email} has logged in`);
  },
  logout: function () {
    console.log(`${this.email} has logged out`);
  },
  listGrades: function () {
    this.grades.forEach((grade) => {
      console.log(grade);
    });
  },
  computeAve: function () {
    const ave = this.grades.reduce((a, b) => a + b) / this.grades.length;
    return ave;
  },
  willPass: function () {
    const ave = Math.round(this.computeAve());

    if (ave >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors: function () {
    const ave = Math.round(this.computeAve());

    if (ave >= 90) {
      return true;
    } else if (ave >= 85 && ave < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let student4 = {
  name: "Jessie",
  email: "jessie@mail.com",
  grades: [91, 89, 92, 93],
  login: function () {
    console.log(`${this.email} has logged in`);
  },
  logout: function () {
    console.log(`${this.email} has logged out`);
  },
  listGrades: function () {
    this.grades.forEach((grade) => {
      console.log(grade);
    });
  },
  computeAve: function () {
    const ave = this.grades.reduce((a, b) => a + b) / this.grades.length;
    return ave;
  },
  willPass: function () {
    const ave = Math.round(this.computeAve());

    if (ave >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors: function () {
    const ave = Math.round(this.computeAve());

    if (ave >= 90) {
      return true;
    } else if (ave >= 85 && ave < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);

// 5.

let classOf1A = {
  students: [student1, student2, student3, student4],
  countHonorStudents: function () {
    let count = 0;

    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        count++;
      }
    });
    return count;
  },
  // Method to count the number of honor students
  countHonorStudents: function () {
    let count = 0;
    for (let student of this.students) {
      if (student.willPassWithHonors()) {
        count++;
      }
    }
    return count;
  },

  // Method to compute the % of honor students
  honorsPercentage: function () {
    const numStudents = this.students.length;
    const numHonorStudents = this.countHonorStudents();
    return (numHonorStudents / numStudents) * 100;
  },
};

console.log(classOf1A.countHonorStudents());
console.log(classOf1A.honorsPercentage());
